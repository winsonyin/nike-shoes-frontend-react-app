import App from './App';
import {render, unmountComponentAtNode} from "react-dom";
import {act} from "react-dom/test-utils";

let container = null;
beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('renders Nike Air Max 95 SE shoe price and state', async () => {
  const shoePrice = {
    "shoePrice": 145
  };

  jest.spyOn(global, 'fetch').mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(shoePrice)
      })
  );

  // eslint-disable-next-line testing-library/no-unnecessary-act
  await act(async () => {
    render(<App />, container)
  });

  // eslint-disable-next-line testing-library/no-node-access
  let elements = container.querySelectorAll('td')

  expect(elements[0].textContent).toBe("Nike Air Max 95 SE");
  expect(elements[1].textContent).toBe("145");
  expect(elements[2].textContent).toBe("Moderate state, can buy now!");
})

it('renders Nike Air Max 97 SE shoe price and state', async () => {
  const shoePrice = {
    "shoePrice": 183
  };

  jest.spyOn(global, 'fetch').mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(shoePrice)
      })
  );

  // eslint-disable-next-line testing-library/no-unnecessary-act
  await act(async () => {
    render(<App />, container)
  });

  // eslint-disable-next-line testing-library/no-node-access
  let elements = container.querySelectorAll('td')

  expect(elements[3].textContent).toBe("Nike Air Max 97 SE");
  expect(elements[4].textContent).toBe("183");
  expect(elements[5].textContent).toBe("Can wait for discount");

})

it('renders Nike Air Max Pre-Day shoe price and state', async () => {
  const shoePrice = {
    "shoePrice": 156
  };

  jest.spyOn(global, 'fetch').mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(shoePrice)
      })
  );

  // eslint-disable-next-line testing-library/no-unnecessary-act
  await act(async () => {
    render(<App />, container)
  });

  // eslint-disable-next-line testing-library/no-node-access
  let elements = container.querySelectorAll('td')

  expect(elements[6].textContent).toBe("Nike Air Max Pre-Day");
  expect(elements[7].textContent).toBe("156");
  expect(elements[8].textContent).toBe("Moderate state, can buy now!");

})

it('renders Nike Air Max 270 shoe price and state', async () => {
  const shoePrice = {
    "shoePrice": 170
  };

  jest.spyOn(global, 'fetch').mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(shoePrice)
      })
  );

  // eslint-disable-next-line testing-library/no-unnecessary-act
  await act(async () => {
    render(<App />, container)
  });

  // eslint-disable-next-line testing-library/no-node-access
  let elements = container.querySelectorAll('td')

  expect(elements[9].textContent).toBe("Nike Air Max 270");
  expect(elements[10].textContent).toBe("170");
  expect(elements[11].textContent).toBe("Can wait for discount");

})

it('renders Nike Renew Ride 3 shoe price and state', async () => {
  const shoePrice = {
    "shoePrice": 114
  };

  jest.spyOn(global, 'fetch').mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(shoePrice)
      })
  );

  // eslint-disable-next-line testing-library/no-unnecessary-act
  await act(async () => {
    render(<App />, container)
  });

  // eslint-disable-next-line testing-library/no-node-access
  let elements = container.querySelectorAll('td')

  expect(elements[12].textContent).toBe("Nike Renew Ride 3");
  expect(elements[13].textContent).toBe("114");
  expect(elements[14].textContent).toBe("Best time to buy!");

})

it('renders Nike Air Max 90 shoe price and state', async () => {
  const shoePrice = {
    "shoePrice": 147
  };

  jest.spyOn(global, 'fetch').mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(shoePrice)
      })
  );

  // eslint-disable-next-line testing-library/no-unnecessary-act
  await act(async () => {
    render(<App />, container)
  });

  // eslint-disable-next-line testing-library/no-node-access
  let elements = container.querySelectorAll('td')

  expect(elements[15].textContent).toBe("Nike Air Max 90");
  expect(elements[16].textContent).toBe("147");
  expect(elements[17].textContent).toBe("Moderate state, can buy now!");

})