### Nike Interview Frontend Code Challenge [React]

_The provided code document should contain more details._

Start static server (Port 3000): `npm install -> npm start`
You need to start backend server (on port 8081), to be able to see live data in the frontend app


#### Component Test Cases
`
npm run test
`
1. renders Nike Air Max 95 SE shoe price and state
2. renders Nike Air Max 97 SE shoe price and state
3. renders Nike Air Max Pre-Day shoe price and state
4. renders Nike Air Max 270 shoe price and state
5. renders Nike Renew Ride 3 shoe price and state
6. renders Nike Air Max 90 shoe price and state

#### Integration Test Cases
`
npm run cypress:open
`
1. Displays the title of the page
2. Displays the Model column
3. Displays the Price column
4. Displays the State column
5. Displays name of Nike Air Max 95 SE
6. Displays price of Nike Air Max 95 SE
7. Displays state of Nike Air Max 95 SE
8. Displays name of Nike Air Max 97 SE
9. Displays price of Nike Air Max 97 SE
10. Displays state of Nike Air Max 97 SE
11. Displays name of Nike Air Max Pre-Day
12. Displays price of Nike Air Max Pre-Day
13. Displays state of Nike Air Max Pre-Day
14. Displays name of Nike Air Max 270
15. Displays price of Nike Air Max 270
16. Displays state of Nike Air Max 270
17. Displays name of Nike Renew Ride 3
18. Displays price of Nike Renew Ride 3
19. Displays state of Nike Renew Ride 3
20. Displays name of Nike Air Max 90
21. Displays price of Nike Air Max 90
22. Displays state of Nike Air Max 90

#### TODO
1. To integrate the new api to get discounted show price, show the discounted shoe price and show the original shoe price
2. To remove the shoeDatabase hard code in the frontend project, and using a new api to get those data from backend
3. To integrate Lint in the .gitlab-ci.yml
4. TO add deploy stage in the .gitlab-ci.yml
5. To support multi-lingual