// listing_shoe-prices.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

describe('Listing all shoe prices', () => {
    it('Displays the title of the page', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > h2');
        cy.should('contain', 'Shoes');
    });

    it('Displays the Model column', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > thead > tr > th:nth-child(1)');
        cy.should('contain', 'Model');
    });

    it('Displays the Price column', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > thead > tr > th:nth-child(2)');
        cy.should('contain', 'Price');
    });

    it('Displays the State column', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > thead > tr > th:nth-child(3)');
        cy.should('contain', 'State');
    });


    // Nike Air Max 95 SE
    it('Displays name of Nike Air Max 95 SE', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(1) > td:nth-child(1)');
        cy.should('contain', 'Nike Air Max 95 SE');
    });

    it('Displays price of Nike Air Max 95 SE', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(1) > td:nth-child(2)');
        cy.should('exist');
    });

    it('Displays state of Nike Air Max 95 SE', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(1) > td:nth-child(3)');
        cy.should('exist');
    });


    // Nike Air Max 97 SE
    it('Displays name of Nike Air Max 97 SE', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(2) > td:nth-child(1)');
        cy.should('contain', 'Nike Air Max 97 SE');
    });

    it('Displays price of Nike Air Max 97 SE', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(2) > td:nth-child(2)');
        cy.should('exist');
    });

    it('Displays state of Nike Air Max 97 SE', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(2) > td:nth-child(3)');
        cy.should('exist');
    });


    // Nike Air Max Pre-Day
    it('Displays name of Nike Air Max Pre-Day', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(3) > td:nth-child(1)');
        cy.should('contain', 'Nike Air Max Pre-Day');
    });

    it('Displays price of Nike Air Max Pre-Day', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(3) > td:nth-child(2)');
        cy.should('exist');
    });

    it('Displays state of Nike Air Max Pre-Day', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(3) > td:nth-child(3)');
        cy.should('exist');
    });


    // Nike Air Max 270
    it('Displays name of Nike Air Max 270', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(4) > td:nth-child(1)');
        cy.should('contain', 'Nike Air Max 270');
    });

    it('Displays price of Nike Air Max 270', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(4) > td:nth-child(2)');
        cy.should('exist');
    });

    it('Displays state of Nike Air Max 270', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(4) > td:nth-child(3)');
        cy.should('exist');
    });


    // Nike Renew Ride 3
    it('Displays name of Nike Renew Ride 3', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(5) > td:nth-child(1)');
        cy.should('contain', 'Nike Renew Ride 3');
    });

    it('Displays price of Nike Renew Ride 3', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(5) > td:nth-child(2)');
        cy.should('exist');
    });

    it('Displays state of Nike Renew Ride 3', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(5) > td:nth-child(3)');
        cy.should('exist');
    });

    // Nike Air Max 90
    it('Displays name of Nike Air Max 90', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(6) > td:nth-child(1)');
        cy.should('contain', 'Nike Air Max 90');
    });

    it('Displays price of Nike Air Max 90', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(6) > td:nth-child(2)');
        cy.should('exist');
    });

    it('Displays state of Nike Air Max 90', () => {
        cy.visit('http://localhost:3000');

        cy.get('#root > div > table > tbody > tr:nth-child(6) > td:nth-child(3)');
        cy.should('exist');
    });
});